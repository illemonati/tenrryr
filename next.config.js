/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    async redirects() {
        return [
            {
                source: "/view/:slug*",
                destination: "/api/view/:slug*",
                permanent: false,
            },
        ];
    },
};

module.exports = nextConfig;
