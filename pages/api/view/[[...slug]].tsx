// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import fs from "fs";
import axios from "axios";
import sharp from "sharp";

type Data = {
    name: string;
};

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Data>
) {
    const jacobPhone = await fs.promises.readFile("assets/jacob-phone.jpg");
    const { slug } = req.query;
    const imageName = slug[0];
    let finalImage = jacobPhone;
    if (imageName) {
        const imageId = /\-(\d+)/.exec(imageName)![1];
        if (fs.existsSync(`cache/${imageId}.jpeg`)) {
            finalImage = await fs.promises.readFile(`cache/${imageId}.jpeg`);
        } else {
            const apiKey = "LIVDSRZULELA"; // from docs, no clue limits on this key
            const resp = await axios.get(
                `https://g.tenor.com/v1/gifs?ids=${imageId}&key=${apiKey}`
            );
            const imageURL = resp.data.results[0].media[0].gif.preview;
            console.log(imageURL);
            const imageGifResp = await axios.get(imageURL, {
                responseType: "arraybuffer",
            });
            const imageGif = imageGifResp.data;
            const processedGif = await sharp(imageGif, { animated: false })
                .resize(300, 300)
                .toBuffer();
            // const phoneGif = await sharp(jacobPhone, { animated: false })
            //     .toFormat("gif")
            //     .toBuffer();
            const compositeGif = await sharp(jacobPhone, { animated: false })
                .composite([
                    {
                        input: processedGif,
                        //@ts-ignore
                        // animated: true,
                        tile: false,
                        top: 398,
                        left: 135,
                        gravity: "northwest",
                    },
                ])
                .toFormat("jpeg")
                .toBuffer();
            finalImage = compositeGif;
            await fs.promises.writeFile(`cache/${imageId}.jpeg`, finalImage);
        }
    }
    res.setHeader("Content-Type", "image/jpeg");
    res.status(200);
    res.write(finalImage);
    res.end();
}
